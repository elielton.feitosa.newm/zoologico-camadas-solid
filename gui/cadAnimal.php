
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/cad-alt-Animal.css">
    <title>Cadastro de Animais</title>
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png"></a>
            <h2>Cadastro de Animais</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main>
            <div class="flex box-dados-animal">
                <form id="form-cadastro-animal" method="POST">
                    <fieldset>
                        <legend>Dados do Animal</legend>
                        <label for=nome>Nome*</label>
                        <input class="campos-dados" type="text" id="nome" name="nome" value="<?= filter_input(INPUT_GET, 'nome', FILTER_SANITIZE_STRING) ?>">
                        <label for="especie">Tipo*</label>
                        <select class="campos-dados" id="tipo" name="tipo">
                            <option <?php if(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING) == 'Avestruz'){echo 'selected';}else{echo '';} ?>>Avestruz</option>
                            <option <?php if(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING) == 'Macaco'){echo 'selected';}else{echo '';} ?>>Macaco</option>
                            <option <?php if(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING) == 'Onça'){echo 'selected';}else{echo '';} ?>>Onça</option>
                            <option <?php if(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING) == 'Hipopotamo'){echo 'selected';}else{echo '';} ?>>Hipopotamo</option>
                            <option <?php if(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING) == 'Urso'){echo 'selected';}else{echo '';} ?>>Urso</option>
                            <option <?php if(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING) == 'Girafa'){echo 'selected';}else{echo '';} ?>>Girafa</option>
                        </select>
                        <label for="tamanho">Comprimento(cm)*</label>
                        <input class="campos-dados" id="comprimento" type="text" name="comprimento" value="<?= filter_input(INPUT_GET, 'tamanho', FILTER_SANITIZE_STRING) ?>">
                        <lable for="peso">Peso(kg)*</lable>
                        <input class="campos-dados" type="peso" id="peso" name="peso" value="<?= filter_input(INPUT_GET, 'peso', FILTER_SANITIZE_STRING) ?>">
                        <input type="button" onClick="enviaCadastroAnimal()" class="btn-form btn-cadastra" name="btn-cadastra" value="Cadastrar">
                        <a href="?pagina=Home"><input class="btn-form btn-voltar" type="button" value="Voltar"></a>
                        <div class="flex resposta-cadastro"><p id="resposta-conteudo"></p></div>
                    </fieldset>
                </form>
            </div>
        </main>
    </div>
    <script type="text/javascript" src="public/js/ajaxAnimal.js"></script>
</body>
</html>