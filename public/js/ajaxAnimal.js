/**********CADASTRO DE ANIMAIS**********/
var form = document.getElementById("form-cadastro-animal");
var obj;
function enviaCadastroAnimal(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById('resposta-conteudo').innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/AnimalDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("cadastra=true&nome=" + form.nome.value + "&tipo=" + form.tipo.value + "&tamanho=" + form.comprimento.value + "&peso=" + form.peso.value);
    }catch(e){
        alert("Erro: " + e.message);
    }
}




/**********EXCLUI ANIMAL**********/
function excluirAnimal(id){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                alert(obj.responseText);
                location.reload();
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/AnimalDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("excluir=true&&id=" + id);
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********ALTERA CADASTRO ANIMAL**********/
var formAlt = document.getElementById("form-altera-animal");
function alteraAnimal(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById('resposta-conteudo').innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/AnimalDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("altera=true&id=" + formAlt.id.value + "&nome=" + formAlt.nome.value + "&tipo=" + formAlt.tipo.value + "&tamanho=" + formAlt.comprimento.value + "&peso=" + formAlt.peso.value);
    }catch(e){
        alert("Erro: " + e.message);
    }
}







