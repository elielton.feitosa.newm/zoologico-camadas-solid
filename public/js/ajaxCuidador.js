/**********CADASTRO DE CUIDADORES**********/
var formCuidador = document.getElementById("form-cadastro-cuidador");
function enviaCadastroCuidador(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById("resposta-conteudo").innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/CuidadorDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("cadastro=true&nome=" + formCuidador.nome.value + "&cpf=" + formCuidador.cpf.value + "&responsavel=" + formCuidador.responsavel.value)
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********EXCLUI CUIDADOR**********/
function excluirCuidador(id){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                alert(obj.responseText);
                location.reload();
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/CuidadorDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("excluir=true&id=" + id);
    }catch(e){
        alert("Erro: " + e.message);
    }
}




/**********ALTERAR CUIDADORES**********/
var formAlteraCuidador = document.getElementById("form-altera-cuidador");
function alteraCuidador(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById("resposta-conteudo").innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/CuidadorDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("altera=true&id=" + formAlteraCuidador.id.value + "&responsavel=" + formAlteraCuidador.responsavel.value)
    }catch(e){
        alert("Erro: " + e.message);
    }
}