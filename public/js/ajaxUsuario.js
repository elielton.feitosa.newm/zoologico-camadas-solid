/**********CADASTRO DE USUÁRIOS**********/
var formUser = document.getElementById("form-cadastro-usuario");
function enviaCadastroUsuario(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById("resposta-conteudo").innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/UsuarioDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("cadastro=true&nome=" + formUser.nome.value + "&usuario=" + formUser.usuario.value + "&senha=" + formUser.senha.value + "&tipo=" + formUser.tipo.value)
    }catch(e){
        alert("Erro: " + e.message);
    }
}




/**********EXCLUI USUÁRIO**********/
function excluirUsuario(user){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                alert(obj.responseText);
                location.reload();
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/UsuarioDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("excluir=true&user=" + user);
    }catch(e){
        alert("Erro: " + e.message);
    }
}




/**********ALTERA CADASTRO USUÁRIO**********/
var formAltUser = document.getElementById("form-altera-usuario");
function alteraUsuario(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById('resposta-conteudo').innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasSolid/business/UsuarioDados.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("altera=true&usuario=" + formAltUser.usuario.value+ "&tipo=" + formAltUser.tipo.value + "&senha=" + formAltUser.senha.value);
    }catch(e){
        alert("Erro: " + e.message);
    }
}
