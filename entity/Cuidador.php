<?php

    class Cuidador{
        private $id;
        private $nome;
        private $cpf;
        private $responsavel;

        public function getId(){
            return $this->id;
        }

        public function getNome(){
            return $this->nome;
        }

        public function getCPF(){
            return $this->cpf;
        }

        public function getResponsavel(){
            return $this->responsavel;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function setCPF($cpf){
            $this->cpf = $cpf;
        }

        public function setResponsavel($responsavel){
            $this->responsavel = $responsavel;
        }
    }
?>