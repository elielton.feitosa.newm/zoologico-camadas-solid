<?php
    class Animal{
        private $id;
        private $nome;
        private $tipo;
        private $tamanho;
        private $peso;

        public function getId(){
            return $this->id;
        }

        public function getNome(){
            return $this->nome;
        }

        public function getTipo(){
            return $this->tipo;
        }

        public function getTamanho(){
            return $this->tamanho;
        }

        public function getPeso(){
            return $this->peso;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function setTipo($tipo){
            $this->tipo = $tipo;
        }

        public function setTamanho($tamanho){
            $this->tamanho = $tamanho;
        }

        public function setPeso($peso){
            $peso = str_replace(',', '.', $peso);
            $this->peso = $peso;
        }
    }
?>