<?php
    define('CWU', 'C:/xampp/htdocs/TreinamentoNewM/zoologicoCamadasSolid/');
    
    //dependencias
    require_once(CWU . 'dal/PostgresConecta.php');
    require_once(CWU . 'dal/UsuarioCRUD.php');
    require_once(CWU . 'business/ValidaUser.php');
    require_once(CWU . 'entity/Usuario.php');
    
    $conexao = new PostgresConecta();//objeto de conexão com bd
    $usuarioCRUD = new UsuarioCRUD($conexao);//objeto para manipulação dos dados do bd
    $validaUser = new ValidaUser();//objeto para validação


    //Cadastro de usuários
    if(isset($_POST['cadastro'])){
        $usuario = new Usuario();
        $usuario->setNome(strip_tags(filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING)));
        $usuario->setUsuario(strip_tags(filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING)));
        $usuario->setSenha(strip_tags(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING)));
        $usuario->setNivel(strip_tags(filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING)));

        try{
            $resultado = $validaUser->cadastro($usuario);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }

        if($resultado == 1){

            $resultado = $usuarioCRUD->verificaUsuarioExiste($usuario);

            switch($resultado){
                case 1:
                    $usuario->setSenha(md5($usuario->getSenha()));
                    $resultado = $usuarioCRUD->cadastraUsuario($usuario);
                    
                    if($resultado == 1){
                        echo 'Cadastrado com sucesso';
                    }if($resultado == -1){
                        echo 'Erro ao cadastrar usuário';
                    }

                break;
                case -1:
                    echo 'Erro ao verificar se o usuário existe';
                break;
                case -2:
                    echo 'Este usuário já existe, por favor cadastre outro';
                break;
            }
        }
    
    }


    //exclusão de usuários
    if(isset($_POST['excluir'])){
        $user = strip_tags(filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING));
        
        $resultado = $usuarioCRUD->excluirUsuario($user);

        switch($resultado){
            case 1:
                echo 'Excluido com sucesso';
            break;
            case -1:
                echo 'Erro ao excluir usuário';
            break;
        }
    }


    //update de usuários
    if(isset($_POST['altera'])){
        $resultado = '';
        $usuario = new Usuario();
        $usuario->setUsuario(strip_tags(filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING)));
        $usuario->setSenha(strip_tags(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING)));
        $usuario->setNivel(strip_tags(filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING)));

        try{
            $resultado = $validaUser->alteracao($usuario);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }

        if($resultado == 1){
            if($usuario->getSenha() != ''){    
                if(strlen($usuario->getSenha()) > 7 && strlen($usuario->getSenha()) < 21){
                    $usuario->setSenha(md5($usuario->getSenha()));
                }else{
                    echo 'Senha deve ter entre 8 e 20 caracteres';
                }
            }

            $resultado = $usuarioCRUD->alteraUsuario($usuario);

            switch($resultado){
                case 1:
                    echo 'Alterado com sucesso';
                break;
                case -1:
                    echo 'Erro ao alterar';
                break;
            }
        }

    }


    //validação de dados do usuário
    function chamaValidacaoLogin($user, $pass){
        $conexao = new PostgresConecta();
        $usuarioCRUD = new UsuarioCRUD($conexao);
        $validaUser = new ValidaUser();
        $usuario = new Usuario();
        $usuario->setUsuario($user);
        $usuario->setSenha($pass);
        //$resultado = $validaDadosUser->validaLogin($usuario);
        $resultado = $validaUser->login($usuario);

        if($resultado == 1){
            $usuario->setSenha(md5($usuario->getSenha()));   
            $resultado = $usuarioCRUD->verificaUsuario($usuario);
        }

        switch($resultado){
            case -1:
                throw new Exception('Erro validar o usuário');
            break;
            case -2:
                throw new Exception('Usuário ou senha incorretos');
            break;
        }


        return $resultado;

    }


    //select da lista de usuarios
    function chamaListaUsuarios(){
        $conexao = new PostgresConecta();
        $usuarioCRUD = new UsuarioCRUD($conexao);
        $_REQUEST['usuarios'] = $usuarioCRUD->listaTodos();
    }
?>