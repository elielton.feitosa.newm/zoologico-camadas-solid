<?php

    require_once('IValidarComLogin.php');

    class ValidaUser implements IValidarComLogin{
    
        public function cadastro($usuario){

            if(strlen($usuario->getUsuario()) > 15 || strlen($usuario->getUsuario()) < 4){
                throw new Exception('Usuário deve ter entre 4 e 15 caracteres');
            }if($usuario->getNivel() != 1 && $usuario->getNivel() != 2){
                throw new Exception('Selecione um tipo de usuário válido');
            }if(strlen($usuario->getNome()) > 50 || strlen($usuario->getNome()) < 4){
                throw new Exception('Nome deve ter entre 4 e 50 caracteres');
            }if(strlen($usuario->getUsuario()) == '' || strlen($usuario->getSenha()) == '' || strlen($usuario->getNivel()) == ''){
                throw new Exception('Preencha todos os campos');
            }if(strlen($usuario->getSenha()) > 20 || strlen($usuario->getSenha()) < 8){
                throw new Exception('Senha deve ter entre 8 e 20 caracteres');
            }

            return 1;
        }

        public function alteracao($usuario){
            if(strlen($usuario->getUsuario()) > 15 || strlen($usuario->getUsuario()) < 4){
                throw new Exception('Usuário deve ter entre 4 e 15 caracteres');
            }if($usuario->getNivel() != 1 && $usuario->getNivel() != 2){
                throw new Exception('Selecione um tipo de usuário válido');
            }if($usuario->getUsuario() == '' || $usuario->getNivel() == ''){
                throw new Exception('Os campos (Usuário e tipo) devem ser preenchidos');
            }

            return 1;
        }

        public function login($usuario){
            if(strlen($usuario->getUsuario()) <= 0 || strlen($usuario->getSenha()) <= 0){
                throw new Exception('Preencha todos os campos');
            }if(strlen($usuario->getUsuario()) < 4 || strlen($usuario->getUsuario()) > 15){
                throw new Exception('Usuário incorreto');
            }if(strlen($usuario->getSenha()) < 8 || strlen($usuario->getSenha()) > 20){
                throw new Exception('Senha incorreta');
            }
        
            return 1;
            
        }
    }
?>