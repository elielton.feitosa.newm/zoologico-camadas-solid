<?php

    require_once('IValidar.php');

    class ValidaAnimal implements IValidar{
        public function cadastro($animal){

            if($animal->getNome() == '' || $animal->getTipo() == '' || $animal->getTamanho() == '' || $animal->getPeso() == ''){
                throw new Exception('Preencha todos os campos');
            }if(strlen($animal->getNome()) > 10 || strlen($animal->getNome()) < 4){
                throw new Exception('Nome deve ter entre 4 e 10 caracteres');
            }

            return 1;
        }

        public function alteracao($animal){
            
            if($animal->getId() == '' || $animal->getTamanho() == '' || $animal->getPeso() == ''){
                throw new Exception('Preencha todos os campos');
            }
            
            return 1;

        }
    }
?>