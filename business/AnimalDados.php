<?php

    define('CWN', 'C:/xampp/htdocs/TreinamentoNewM/zoologicoCamadasSolid/');
    require_once(CWN . 'dal/MysqlConecta.php');
    require_once(CWN . 'dal/AnimalCRUD.php');
    require_once(CWN . 'business/ValidaAnimal.php');
    require_once(CWN . 'entity/Animal.php');
    
    $conecta = new MysqlConecta();
    $animalCRUD = new AnimalCRUD($conecta);
    $validaAnimal = new ValidaAnimal();

    $resultado = 0;
    $msg = "";
    


    //cadastro de animais
    if(isset($_POST['cadastra'])){
        $animal = new Animal();
        $animal->setNome(strip_tags($_POST['nome']));
        $animal->setTipo(strip_tags($_POST['tipo']));
        $animal->setTamanho(strip_tags($_POST['tamanho']));
        $animal->setPeso(strip_tags($_POST['peso']));

        try{
            $resultado = $validaAnimal->cadastro($animal);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }

        if($resultado == 1){
            $resultado = $animalCRUD->cadastraAnimal($animal);

            switch($resultado){
                case 1:
                    echo 'Cadastrado com sucesso';
                break;
                case -1:
                    echo 'Erro ao cadastrar';
                break;
            }

        }

    }



    if(isset($_POST['altera'])){
        $animal = new Animal();
        $animal->setId(strip_tags(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING)));
        $animal->setTamanho(strip_tags(filter_input(INPUT_POST, 'tamanho', FILTER_SANITIZE_STRING)));
        $animal->setPeso(strip_tags(filter_input(INPUT_POST, 'peso', FILTER_SANITIZE_STRING)));

        try{
            $resultado = $validaAnimal->alteracao($animal);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }

        if($resultado == 1){
            $resultado = $animalCRUD->alteraAnimal($animal);

            switch($resultado){
                case 1:
                    echo 'Alterado com sucesso';
                break;
                case -1:
                    echo 'Erro ao alterar';
                break;
            }
        }
    }



    if(isset($_POST['excluir'])){
        $id = strip_tags(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING));

        $resultado = $animalCRUD->excluirAnimal($id);

        switch($resultado){
            case 1:
                echo 'Excluido com sucesso';
            break;
            case -1:
                echo 'Erro ao excluir';
            break;
        }

    }

    

    function chamaListaAnimais(){
        $conecta = new MysqlConecta();
        $animalCRUD = new AnimalCRUD($conecta);
        $_REQUEST['animais'] = $animalCRUD->listaTodos();
    }
    
?>