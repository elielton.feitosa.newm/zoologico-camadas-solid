<?php
    define('CWC', 'C:/xampp/htdocs/TreinamentoNewM/zoologicoCamadasSolid/');
    require_once(CWC . 'dal/MysqlConecta.php');
    require_once(CWC . 'dal/CuidadorCRUD.php');
    require_once('ValidaCuidador.php');
    require_once(CWC . 'entity/Cuidador.php');

    $conexao = new MysqlConecta();
    $cuidadorCRUD = new CuidadorCRUD($conexao);
    $validaCuidador = new ValidaCuidador();
    
    if(isset($_POST['cadastro'])){
        $cuidador = new Cuidador();
        $cuidador->setNome(strip_tags(filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING)));
        $cpf = strip_tags(filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING));
        $cpf = str_replace('.', '', $cpf);
        $cpf = str_replace('-', '', $cpf);
        $cuidador->setCPF($cpf);
        $cuidador->setResponsavel(strip_tags(filter_input(INPUT_POST, 'responsavel', FILTER_SANITIZE_STRING)));

        try{
            $resutlado = $validaCuidador->cadastro($cuidador);

            if($resutlado == 1){
                $resultado = $cuidadorCRUD->cadastrar($cuidador);

                switch($resultado){
                    case 1:
                        echo 'Cadastrado com sucesso';
                    break;
                    case -1:
                        echo 'Erro ao cadastrar';
                    break;
                }
            }

        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }

    if(isset($_POST['altera'])){
        $cuidador = new Cuidador();
        $cuidador->setId(strip_tags(filter_input(INPUT_POST, 'id')));
        $cuidador->setResponsavel(strip_tags(filter_input(INPUT_POST, 'responsavel', FILTER_SANITIZE_STRING)));

        try{
            $resultado = $validaCuidador->alteracao($cuidador);

            if($resultado){
                $resultado = $cuidadorCRUD->alterar($cuidador);

                switch($resultado){
                    case 1:
                        echo 'Alterado com sucesso';
                    break;
                    case -1:
                        echo 'Erro ao alterar';
                    break;
                }
            }
        }catch(Exception $ex){
            echo $ex->getMessage();
        }

    }

    if(isset($_POST['excluir'])){
        $cuidador = new Cuidador();
        $cuidador->setId(strip_tags(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING)));
        
        $resultado = $cuidadorCRUD->excluir($cuidador);

        switch($resultado){
            case 1:
                echo 'Excluido com sucesso';
            break;
            case -1:
                echo 'Erro ao excluir';
            break;
        }
    }

    function chamaListaCuidadores(){
        $conexao = new MysqlConecta();
        $cuidadorCRUD = new CuidadorCRUD($conexao);
        $_REQUEST['cuidadores'] = $cuidadorCRUD->listaTodos();
    }

?>