<?php
    require_once('IValidar.php');

    class ValidaCuidador implements IValidar{
        
        private $cuidadorDal;

        public function cadastro($cuidador){
            if(strlen($cuidador->getNome()) == 0 || strlen($cuidador->getResponsavel()) == 0 || strlen($cuidador->getCpf()) == 0){
                throw new Exception('Preencha todos os Campos');
            }if(strlen($cuidador->getNome()) < 4 || strlen($cuidador->getNome()) > 50){
                throw new Exception('Nome deve ter entre 4 e 50 caracteres');
            }if(strlen($cuidador->getCpf()) != 11){
                throw new Exception('Cpf inválido');
            }

            return 1;
            
        }

        public function alteracao($cuidador){
            if($cuidador->getId() < 1){
                throw new Exception('Id inválido');
            }

            return 1;
            
        }

    }

?>