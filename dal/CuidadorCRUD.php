<?php

    class CuidadorCRUD{
        private $conexao;

        public function __construct(IConexaoBD $conexao){
            $this->conexao = $conexao;
        }

        public function cadastrar($cuidador){
            $nome = $cuidador->getNome();
            $cpf = $cuidador->getCPF();
            $responsavel = $cuidador->getResponsavel();

            try{
                $conexao = $this->conexao->conecta();
                $sql = 'INSERT INTO cuidador(nome, cpf, responsavel) VALUES (:nome, :cpf, :responsavel)';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':nome', $nome);
                $stmt->bindParam(':cpf', $cpf);
                $stmt->bindParam(':responsavel', $responsavel);
                
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function listaTodos(){
            try{
                $conexao = $this->conexao->conecta();
                $sql = 'SELECT * FROM cuidador';
                $stmt = $conexao->prepare($sql);
                $resultado = $stmt->execute();
                $cuidadores = $stmt->fetchAll(PDO::FETCH_ASSOC);

                if($resultado){
                    return $cuidadores;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function alterar($cuidador){
            $id = $cuidador->getId();
            $responsavel = $cuidador->getResponsavel();
            try{
                $conexao = $this->conexao->conecta();
                $sql = 'UPDATE cuidador SET responsavel = :responsavel WHERE id = :id';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':responsavel', $responsavel);
                $stmt->bindParam(':id', $id);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function excluir($cuidador){
            $id = $cuidador->getId();
            try{
                $conexao = $this->conexao->conecta();
                $sql = 'DELETE FROM cuidador WHERE id = :id';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':id', $id);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }
    }

?>