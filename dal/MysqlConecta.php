<?php

    require_once('IConexaoBD.php');

    class MysqlConecta implements IConexaoBD{

        private $server = 'localhost';
        private $user = 'root';
        private $password = '';
        private $db = 'zoologico';

        public function conecta(){
            try{
                $conexao = new PDO("mysql:host={$this->server}; dbname={$this->db}; charset=utf8", $this->user, $this->password);
                return $conexao;
            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }
    }
?>