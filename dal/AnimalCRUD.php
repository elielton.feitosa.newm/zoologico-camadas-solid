<?php
    
    class AnimalCRUD{

        private $conexao;

        public function __construct(IConexaoBD $conexao){
            $this->conexao = $conexao;
        }

        public function cadastraAnimal(Animal $animal){
            $nome = $animal->getNome();
            $tipo = $animal->getTipo();
            $tamanho = $animal->getTamanho();
            $peso = $animal->getPeso();
            
            try{
                $conexao = $this->conexao->conecta();
                $sql = 'INSERT INTO animal(nome, tipo, tamanho, peso) VALUES (:nome, :tipo, :tamanho, :peso)';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(":nome", $nome);
                $stmt->bindParam(":tipo", $tipo);
                $stmt->bindParam(":tamanho", $tamanho);
                $stmt->bindParam(":peso", $peso);
                $result = $stmt->execute();

                if($result){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function listaTodos(){
            try{
                $conexao = $this->conexao->conecta();
                $sql = 'SELECT * FROM animal';
                $stmt = $conexao->prepare($sql);
                $result = $stmt->execute();

                if($result){
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                }else{
                    return -1;
                }
            }catch(PDOException $ex){
                echo "Erro: " . $ex->getMessage();
            }
        }

        public function excluirAnimal($id){
            try{
                $conexao = $this->conexao->conecta();
                $sql = 'DELETE FROM animal WHERE id = :id';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':id', $id);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }
            }catch(PDOException $ex){
                echo "Erro: " . $ex->getMessage();
            }
        }

        public function alteraAnimal($animal){
            $id = $animal->getId();
            $tamanho = $animal->getTamanho();
            $peso = $animal->getPeso();
            
            try{
                $conexao = $this->conexao->conecta();
                $sql = 'UPDATE animal SET tamanho = :tamanho, peso = :peso WHERE id = :id';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':tamanho', $tamanho);
                $stmt->bindParam(':peso', $peso);
                $stmt->bindParam(':id', $id);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }
            }catch(PDOException $ex){
                echo "Erro: " . $ex->getMessage();
            }
        }
    }
?>